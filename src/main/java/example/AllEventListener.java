package example;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import example.api.MyPluginComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by daniel on 4/16/17.
 */
@ExportAsService
@Named("allEventListener")
public class AllEventListener implements LifecycleAware
{
    private final Logger logger = LoggerFactory.getLogger( AllEventListener.class);


    @ComponentImport
    private final EventPublisher eventPublisher;

    @Inject
    public AllEventListener( final EventPublisher eventPublisher)
    {
        this.eventPublisher = eventPublisher;
    }


    public void onStart()
    {
        this.eventPublisher.register( this );
    }

    public void onStop()
    {
        this.eventPublisher.unregister( this );
    }

    @EventListener
    public void listenForAllEvents( Object event)
    {
        logger.error("Received an event for " + event.getClass().getName());
    }
}
